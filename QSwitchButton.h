#ifndef QSWITCHBUTTON_H
#define QSWITCHBUTTON_H


#include <QCheckBox>
#include <QColor>
#include <QLinearGradient>


class QSwitchButton : public QWidget {
    Q_OBJECT
    Q_DISABLE_COPY(QSwitchButton)
public:
    explicit QSwitchButton(QWidget* parent = nullptr);
    ~QSwitchButton() override;

    //-- Setters
    void setAnimationDuration(int duration);


    void setState(Qt::CheckState state);
    void setStateAnimated(Qt::CheckState state);

    void setValue(bool val);
    void setValueAnimated(bool val);
    //-- Getters
    bool value() const {return m_state == Qt::CheckState::Checked;}
    Qt::CheckState state() const {return m_state;}

    void setOnText(const QString &text);
    void setOffText(const QString &text);

    void setOnBgGradient(const QColor &col1, const QColor &col2);
    void setOffBgGradient(const QColor &col1, const QColor &col2);
    void setDisabledOnBgGradient(const QColor &col1, const QColor &col2);
    void setDisabledOffBgGradient(const QColor &col1, const QColor &col2);

    void setOnTextColor(const QColor &col);
    void setOffTextColor(const QColor &col);

    void setOnTextCustomFont(const QFont &font);
    void setOffTextCustomFont(const QFont &font);

    void clearOnTextCustomFont();
    void clearOffTextCustomFont();

    void updateSizes();
Q_SIGNALS:
    void stateChanged(Qt::CheckState state);
    void valueChanged(bool value);
protected:
    //-- QWidget methods
    void mousePressEvent(QMouseEvent *event) override;
    void paintEvent(QPaintEvent* event) override;
private:
    class SwitchMoveAnimation;
    class SwitchCircle;
    class SwitchBackground;


private:
    Qt::CheckState m_state {Qt::CheckState::Unchecked};
    int  m_animation_duration {100};

    const QList<SwitchMoveAnimation *> &moveAnimations() const {return m_moveAnimations;}


    QColor m_border_pen;
    QLinearGradient m_border_lg;
    int    m_borderradius {12};

    // This order for definition is important (these widgets overlap)
    SwitchBackground *m_back_on;
    SwitchBackground *m_back_off;
    SwitchCircle     *m_circle;

    SwitchMoveAnimation *m_back_on_move;
    SwitchMoveAnimation *m_circle_move;
    QList<SwitchMoveAnimation *> m_moveAnimations;

    QMargins m_textMargins {1,1,1,1};
};


#endif // SWITCHBUTTON_H
