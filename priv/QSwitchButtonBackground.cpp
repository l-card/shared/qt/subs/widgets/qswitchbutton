#include "QSwitchButtonBackground.h"
#include <QPainter>
#include <QPen>

QSwitchButton::SwitchBackground::SwitchBackground(QWidget* parent) : QWidget{parent},
    m_border_color_en{QColor{150, 150, 150}},
    m_border_color_dis{QColor{150, 150, 150}},
    m_text_color{QColor{255,255,255}},
    m_text_offs{12},
    m_lg_en{stdOnGradient(QColor{154, 194, 50}, QColor{154, 210, 50})},
    m_lg_dis{stdOnGradient(QColor{190, 190, 190}, QColor{230, 230, 230})} {


}

QSwitchButton::SwitchBackground::~SwitchBackground() {
}



void QSwitchButton::SwitchBackground::paintEvent(QPaintEvent*) {
  QPainter painter(this);
  painter.save();
  painter.setRenderHint(QPainter::Antialiasing, true);

  QPen pen(Qt::NoPen);
  painter.setPen(pen);

  painter.setBrush(isEnabled() ? m_border_color_en : m_border_color_dis);
  painter.drawRoundedRect(0, 0, width(), height(), 10, 10);
  painter.setBrush(isEnabled() ? m_lg_en : m_lg_dis);
  painter.drawRoundedRect(1, 1, width()-2, height()-2, 8, 8);

  QPen textPen{m_text_color};
  painter.setPen(textPen);

  if (m_text_custom_font_en) {
    painter.setFont(m_text_custom_font);
  }
  int text_rect_width =  width() - m_text_offs - 8;
  if (text_rect_width > 0) {
      painter.drawText(QRect(m_text_offs, 0, text_rect_width, height()), Qt::AlignVCenter | Qt::AlignLeft, m_text);
  }
  painter.restore();
}

QLinearGradient QSwitchButton::SwitchBackground::stdOnGradient(
        const QColor &col1, const QColor &col2) {
    QLinearGradient gr{0, 25, 70, 0};
    gr.setColorAt(0, col1);
    gr.setColorAt(0.25, col2);
    gr.setColorAt(0.95, col1);
    return gr;
}

QLinearGradient QSwitchButton::SwitchBackground::stdOffGradient(const QColor &col1, const QColor &col2) {
    QLinearGradient gr{50, 30, 35, 0};
    gr.setColorAt(0,     col1);
    gr.setColorAt(0.25,  col2);
    gr.setColorAt(0.82,  col2);
    gr.setColorAt(1,     col1);
    return gr;
}
