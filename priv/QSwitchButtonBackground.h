#ifndef SWITCHBUTTONBACKGROUND_H
#define SWITCHBUTTONBACKGROUND_H

#include "../QSwitchButton.h"

class QSwitchButton::SwitchBackground : public QWidget {
    Q_OBJECT
    Q_DISABLE_COPY(SwitchBackground)

public:
    explicit SwitchBackground(QWidget* parent = nullptr);
    ~SwitchBackground() override;

    const QString &text() const {return m_text;}

    void setBorderColor(QColor col) { m_border_color_en =  col;}
    void setGradient(const QLinearGradient &gr) {m_lg_en = gr;}
    void setGradientDisabled(const QLinearGradient &gr) {m_lg_dis = gr;}

    void setText(const QString &text) {m_text = text;}
    void setTextOffset(int offs) {m_text_offs = offs;}
    void setTextColor(const QColor &col) {m_text_color = col;}
    void setTextCustomFont(const QFont &font) {m_text_custom_font = font; m_text_custom_font_en = true;}
    void clearTextCustomFont() {m_text_custom_font_en = false;}

    bool isTextCustomFontEnabled() const {return m_text_custom_font_en;}
    const QFont textCustomFont() const {return m_text_custom_font;}



    static QLinearGradient stdOnGradient(const QColor &col1, const QColor &col2);
    static QLinearGradient stdOffGradient(const QColor &col1, const QColor &col2);
protected:
    void paintEvent(QPaintEvent *event) override;
private:


    QColor m_border_color_en;
    QColor m_border_color_dis;


    QLinearGradient m_lg_en;
    QLinearGradient m_lg_dis;

    QString  m_text;
    QColor   m_text_color;
    QFont    m_text_custom_font;
    bool     m_text_custom_font_en {false};
    int m_text_offs;
};
#endif // SWITCHBUTTONBACKGROUND_H
