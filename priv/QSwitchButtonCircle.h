#ifndef SWITCHBUTTONCIRCLE_H
#define SWITCHBUTTONCIRCLE_H

#include "../QSwitchButton.h"
#include <QRadialGradient>

class QSwitchButton::SwitchCircle : public QWidget {
    Q_OBJECT
    Q_DISABLE_COPY(SwitchCircle)
public:
    explicit SwitchCircle(QWidget* parent = nullptr);
    ~SwitchCircle() override;
protected:
    void paintEvent(QPaintEvent* event) override;
private:
    bool            m_rect;
    int             m_borderradius;
    QColor          m_color_pen;
    QRadialGradient m_rg;
    QLinearGradient m_lg;
    QLinearGradient m_lg_disabled;
};


#endif // SWITCHBUTTONCIRCLE_H
