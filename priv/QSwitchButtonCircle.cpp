#include "QSwitchButtonCircle.h"
#include <QPainter>
#include <QPen>

QSwitchButton::SwitchCircle::SwitchCircle(QWidget* parent): QWidget(parent),
    m_color_pen(QColor(120, 120, 120)) {

    setFixedSize(20, 20);

    m_rg = QRadialGradient(static_cast<int>(width() / 2), static_cast<int>(height() / 2), 12);
    m_rg.setColorAt(0, QColor(255, 255, 255));
    m_rg.setColorAt(0.6, QColor(255, 255, 255));
    m_rg.setColorAt(1, QColor(205, 205, 205));

    m_lg = QLinearGradient(3, 18, 20, 4);
    m_lg.setColorAt(0, QColor(255, 255, 255));
    m_lg.setColorAt(0.55, QColor(230, 230, 230));
    m_lg.setColorAt(0.72, QColor(255, 255, 255));
    m_lg.setColorAt(1, QColor(255, 255, 255));

    m_lg_disabled = QLinearGradient(3, 18, 20, 4);
    m_lg_disabled.setColorAt(0, QColor(230, 230, 230));
    m_lg_disabled.setColorAt(0.55, QColor(210, 210, 210));
    m_lg_disabled.setColorAt(0.72, QColor(230, 230, 230));
    m_lg_disabled.setColorAt(1, QColor(230, 230, 230));
}

QSwitchButton::SwitchCircle::~SwitchCircle() {
}

void QSwitchButton::SwitchCircle::paintEvent(QPaintEvent*) {
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    QPen pen(Qt::NoPen);
    painter.setPen(pen);
    painter.setBrush(m_color_pen);

    int d = width();
    int offs = 0;

    painter.drawEllipse(0, 0, d, d);

    offs+=1;
    painter.setBrush(m_rg);
    painter.drawEllipse(offs, offs, d - 2*offs, d - 2 *offs);

    offs +=1;
    painter.setBrush(QColor(210, 210, 210));
    painter.drawEllipse(offs, offs, d - 2*offs, d - 2 *offs);

    offs +=1;
    if (isEnabled()) {
        painter.setBrush(m_lg);
        painter.drawEllipse(offs, offs, d - 2*offs, d - 2 *offs);
    } else {
        painter.setBrush(m_lg_disabled);
        painter.drawEllipse(offs, offs, d - 2*offs, d - 2 *offs);
    }
}
