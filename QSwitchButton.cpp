#include "QSwitchButton.h"
#include <QPainter>
#include <QPen>
#include <QLinearGradient>
#include <QRadialGradient>
#include <QLabel>
#include <QPropertyAnimation>
#include <QMouseEvent>
#include "priv/QSwitchButtonCircle.h"
#include "priv/QSwitchButtonBackground.h"

/* -------------------------- SwitchMoveAnimation ----------------------------*/
class QSwitchButton::SwitchMoveAnimation  {
public:
    enum class AnimationProperty {
        Pos  = 0x0001,
        Size = 0x0002

    };
    Q_DECLARE_FLAGS(AnimationProperties, AnimationProperty);


    SwitchMoveAnimation(QWidget *widget, AnimationProperties properties, int duration);

    void setPos(Qt::CheckState on, QPoint pos) {
        stateGeometry(on).pos = pos;
    }
    void setSize(Qt::CheckState on, QSize size) {
        stateGeometry(on).size = size;
    }

    void setState(Qt::CheckState on);
    void moveToState(Qt::CheckState state);
    void setDuration(int duration);
private:
    const QList<QPropertyAnimation *> &activeAnimations() const {return m_activeAnimations;}

    QWidget *m_wgt;
    AnimationProperties m_properies;


    struct StateGeometry {
        QPoint pos;
        QSize size;
    };

    StateGeometry  &stateGeometry(Qt::CheckState state) {
        return state == Qt::CheckState::Checked ? m_on :
               state == Qt::CheckState::Unchecked ? m_off : m_tristate;
    }

    StateGeometry m_on, m_off, m_tristate;

    struct {
        QPropertyAnimation *pos;
        QPropertyAnimation *size;
    } m_animation;
    QList<QPropertyAnimation *> m_activeAnimations;

    Qt::CheckState m_state {Qt::CheckState::Unchecked};
};


QSwitchButton::SwitchMoveAnimation::SwitchMoveAnimation(
        QWidget *widget, AnimationProperties properties, int duration) :
    m_wgt{widget}, m_properies{properties} {

    if (properties & AnimationProperty::Pos) {
        m_animation.pos = new QPropertyAnimation{widget};
        m_animation.pos->setTargetObject(widget);
        m_animation.pos->setPropertyName("pos");
        m_animation.pos->setDuration(duration);
        m_activeAnimations.append(m_animation.pos);
    } else {
        m_animation.pos = nullptr;
    }

    if (properties & AnimationProperty::Size) {
        m_animation.size = new QPropertyAnimation{widget};
        m_animation.size->setTargetObject(widget);
        m_animation.size->setPropertyName("size");
        m_animation.size->setDuration(duration);
        m_activeAnimations.append(m_animation.size);
    } else {
        m_animation.size = nullptr;
    }
}

void QSwitchButton::SwitchMoveAnimation::setState(Qt::CheckState on) {
    m_state = on;
    for (QPropertyAnimation *animation : activeAnimations()) {
        animation->stop();
    }

    if (m_properies & AnimationProperty::Pos) {
        m_wgt->move(stateGeometry(m_state).pos);
    }

    if (m_properies & AnimationProperty::Size) {
        m_wgt->resize(stateGeometry(m_state).size);
    }
}

void QSwitchButton::SwitchMoveAnimation::moveToState(Qt::CheckState state) {
    if (m_state != state) {
        for (QPropertyAnimation *animation : activeAnimations()) {
            animation->stop();
        }

        if (m_properies & AnimationProperty::Pos) {
            m_animation.pos->setStartValue(stateGeometry(m_state).pos);
            m_animation.pos->setEndValue(stateGeometry(state).pos);
        }
        if (m_properies & AnimationProperty::Size) {
            m_animation.size->setStartValue(stateGeometry(m_state).size);
            m_animation.size->setEndValue(stateGeometry(state).size);
        }


        for (QPropertyAnimation *animation : activeAnimations()) {
            animation->start();
        }

        m_state = state;
    }
}

void QSwitchButton::SwitchMoveAnimation::setDuration(int duration) {
    for (QPropertyAnimation *animation : activeAnimations()) {
        animation->setDuration(duration);
    }
}




/* ---------------------------- SwitchButton ---------------------------------*/
QSwitchButton::QSwitchButton(QWidget* parent) : QWidget{parent} {

  m_border_pen = QColor{120, 120, 120};

  m_border_lg = QLinearGradient{35, 30, 35, 0};
  m_border_lg.setColorAt(0, QColor{210, 210, 210});
  m_border_lg.setColorAt(0.25, QColor{255, 255, 255});
  m_border_lg.setColorAt(0.82, QColor{255, 255, 255});
  m_border_lg.setColorAt(1, QColor{210, 210, 210});


  m_back_off = new SwitchBackground{this};
  m_back_on = new SwitchBackground{this};
  m_circle = new SwitchCircle{this};

  setWindowFlags(Qt::WindowType::FramelessWindowHint);
  setAttribute(Qt::WidgetAttribute::WA_TranslucentBackground);

  QFont sfont {font()};
  sfont.setBold(true);
  setFont(sfont);


  m_back_on->setText(QStringLiteral("On"));
  m_back_on->setTextColor(QColor{255, 255, 255});

  m_back_off->setText(QStringLiteral("Off"));
  m_back_off->setTextColor(QColor{120, 120, 120});
  m_back_off->setBorderColor(QColor{210, 210, 210});

  m_back_off->setGradient(SwitchBackground::stdOffGradient(
                              QColor{230, 230, 230}, QColor{255, 255, 255}));
  m_back_off->setGradientDisabled(SwitchBackground::stdOffGradient(
                                      QColor{200, 200, 200}, QColor{230, 230, 230}));


  m_moveAnimations.append(m_back_on_move = new SwitchMoveAnimation{m_back_on,  SwitchMoveAnimation::AnimationProperty::Size, m_animation_duration});
  m_moveAnimations.append(m_circle_move = new SwitchMoveAnimation{m_circle,  SwitchMoveAnimation::AnimationProperty::Pos, m_animation_duration});

  updateSizes();
  setValue(false);
}

QSwitchButton::~QSwitchButton() {
  delete m_circle;
  delete m_back_on;
  delete m_back_off;

  qDeleteAll(m_moveAnimations);
}

void QSwitchButton::paintEvent(QPaintEvent*) {
    QPainter  painter(this);

    painter.setRenderHint(QPainter::RenderHint::Antialiasing, true);

    QPen pen{Qt::PenStyle::NoPen};
    painter.setPen(pen);

    painter.setBrush(m_border_pen);
    painter.drawRoundedRect(0, 0, width(), height(), 12, 12);
    painter.setBrush(m_border_lg);
    painter.drawRoundedRect(1, 1, width() - 2, height() - 2, 10, 10);
}

void QSwitchButton::mousePressEvent(QMouseEvent *event) {
    if (isEnabled()) {
        if ((event->button() == Qt::MouseButton::LeftButton) && (event->type() == QEvent::Type::MouseButtonPress)) {
            setValueAnimated(!value());
        }
    }
}

void QSwitchButton::setAnimationDuration(int time) {
    for (SwitchMoveAnimation *an : moveAnimations()) {
        an->setDuration(time);
    }
}

void QSwitchButton::setValue(bool val) {
    setState(val ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
}

void QSwitchButton::setState(Qt::CheckState state) {
    for (SwitchMoveAnimation *an : moveAnimations()) {
        an->setState(state);
    }
    if (m_state != state) {
        m_state = state;
        Q_EMIT stateChanged(state);
        if (state != Qt::CheckState::PartiallyChecked)
            Q_EMIT valueChanged(value());
    }
}

void QSwitchButton::setStateAnimated(Qt::CheckState state) {
    if (m_state != state) {
        m_state = state;
        for (SwitchMoveAnimation *an : moveAnimations()) {
            an->moveToState(state);
        }
        Q_EMIT stateChanged(m_state);
        if (state != Qt::CheckState::PartiallyChecked)
            Q_EMIT valueChanged(value());
    }
}

void QSwitchButton::setValueAnimated(bool val) {
    setStateAnimated(val ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
}

void QSwitchButton::setOnText(const QString &text) {
    m_back_on->setText(text);
}

void QSwitchButton::setOffText(const QString &text) {
    m_back_off->setText(text);
}

void QSwitchButton::setOnBgGradient(const QColor &col1, const QColor &col2) {
    m_back_on->setGradient(SwitchBackground::stdOnGradient(col1, col2));
}

void QSwitchButton::setOffBgGradient(const QColor &col1, const QColor &col2) {
    m_back_off->setGradient(SwitchBackground::stdOffGradient(col1, col2));
}

void QSwitchButton::setDisabledOnBgGradient(const QColor &col1, const QColor &col2) {
    m_back_on->setGradientDisabled(SwitchBackground::stdOnGradient(col1, col2));
}

void QSwitchButton::setDisabledOffBgGradient(const QColor &col1, const QColor &col2) {
    m_back_off->setGradientDisabled(SwitchBackground::stdOffGradient(col1, col2));
}

void QSwitchButton::setOnTextColor(const QColor &col) {
    m_back_on->setTextColor(col);
}

void QSwitchButton::setOffTextColor(const QColor &col) {
    m_back_off->setTextColor(col);
}

void QSwitchButton::setOnTextCustomFont(const QFont &font) {
    m_back_on->setTextCustomFont(font);
}

void QSwitchButton::setOffTextCustomFont(const QFont &font) {
    m_back_off->setTextCustomFont(font);
}

void QSwitchButton::clearOnTextCustomFont() {
    m_back_on->clearTextCustomFont();
}

void QSwitchButton::clearOffTextCustomFont() {
    m_back_off->clearTextCustomFont();
}



void QSwitchButton::updateSizes() {
    static const int border_size = 2;
    static const int text_circle_pad = 6; /* мин. отступ текста от переключателя */
    static const int text_border_pad = 10; /* мин. отступ текста от границы виджета */

    QFontMetrics fm_on  {m_back_on->isTextCustomFontEnabled() ? QFontMetrics{m_back_on->textCustomFont()} : fontMetrics()};
    QFontMetrics fm_off {m_back_off->isTextCustomFontEnabled() ? QFontMetrics{m_back_off->textCustomFont()} : fontMetrics()};
    const int text_height    {qMax(fm_on.height(), fm_off.height())};
    const int text_width_on  {fm_on.boundingRect(m_back_on->text()).width()};
    const int text_width_off {fm_off.boundingRect(m_back_off->text()).width()};
    const int text_width     {qMax(text_width_on, text_width_off)};

    const int back_height    {text_height + m_textMargins.top() + m_textMargins.bottom()};
    const int total_height   {back_height + 2 * border_size};
    const int circle_size    {back_height};
    const int back_width     {text_width + circle_size + text_circle_pad + text_border_pad};
    const int total_width    {back_width + 2 * border_size};


    m_back_on->move(border_size, border_size);
    m_back_on->setFixedHeight(back_height);

    m_back_off->move(border_size, border_size);
    m_back_off->setFixedHeight(back_height);
    m_back_off->setFixedWidth(total_width - 2 * border_size);

    m_circle->setFixedSize(circle_size, circle_size);
    m_circle_move->setPos(Qt::CheckState::Unchecked, QPoint{border_size, border_size});
    m_circle_move->setPos(Qt::CheckState::Checked, QPoint{total_width - border_size - circle_size, border_size});
    m_circle_move->setPos(Qt::CheckState::PartiallyChecked, QPoint{(total_width - circle_size)/2, border_size});

    m_back_on_move->setSize(Qt::CheckState::Unchecked, QSize{circle_size, back_height});
    m_back_on_move->setSize(Qt::CheckState::Checked, QSize{total_width - 2 * border_size, back_height});
    m_back_on_move->setSize(Qt::CheckState::PartiallyChecked, QSize{(total_width - 2 * border_size + circle_size)/2, back_height});


    m_back_on->setTextOffset(text_border_pad + (text_width - text_width_on)/2);
    m_back_off->setTextOffset(circle_size + text_circle_pad + (text_width - text_width_off)/2);

    setFixedSize(QSize{total_width, total_height});
    /* обновляем текущее положение ползунка */
    for (SwitchMoveAnimation *an : moveAnimations()) {
        an->setState(state());
    }
}







